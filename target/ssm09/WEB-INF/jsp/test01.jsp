<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!-- 相对路径 -->
<jsp:include page="../../common/easyui.jsp"></jsp:include>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    	<table id="dg"></table>
 	</body>
 	<script type="text/javascript">
 		var basepath = '<%=request.getContextPath()%>';
 		console.log('basepath=',basepath);
 		$('#dg').datagrid({
		    url:basepath + 'user/userShowlist',
		    method:'get',
		    queryParams:{id:'12'},
		    singleSelect:true,
		    rownumbers:true,
		    pagination:false,
		    pagePosition:'bottom',
		    pageNumber:1,
		    pageSize:10,
		    pageList:[10,20,30,40,50],
		    columns:[[
		    	{field:'id',title:'编号',width:100},
				{field:'userName',title:'用户名',width:100},
				{field:'password',title:'用户密码',width:100},
				{field:'age',title:'年龄',width:100,align:'right'}
		    ]],
		    onLoadSuccess:function(data){
		    	console.log('data===',data)
		    }
		});
 		
 	</script>
</html>