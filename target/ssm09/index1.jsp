<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 相对路径 -->
<%@include file="common/easyui.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div id="tb">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:add('Add')">Add</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true" onclick="javascript:alert('Cut')">Cut</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:save('111')">Save</a>
	</div>
 	<table id="dg"></table>
</body>
<script type="text/javascript">
 		var basepath = '<%=request.getContextPath()%>';
 		console.log('basepath=',basepath);
 		$(function() {
//			var paramObj = {
//		    	pageSize : 10,
//		   		params : ''
//			};
// 			paramObj.queryParameters['page']= 1;
//			paramObj.queryParameters['rows']= paramObj.pageSize;
 			$('#dg').datagrid({
 			    //url:basepath + '/userRest/topage',
	    		//url:basepath + '/userinfo/userShow',
 			    url:basepath + '/userinfo/userShow/pagehelpers',
 			    method:'get',
// 			    queryParams:paramObj.queryParameters,
 			    singleSelect:true,
 			    rownumbers:true,
 			    pagination:true,
 			    pagePosition:'bottom',
 			    pageNumber:1,
 			    pageSize:10,
 			    pageList:[10,20,30,40,50],
 			    autoRowHeight:false,
 			    fitColumns:true,
 			    columns:[[
 			    	{field:'id',title:'编号',width:100,align:'center'},
 					{field:'name',title:'用户名',width:100,align:'center'},
 					{field:'address',title:'地址',width:100,align:'center'},
 					{field:'age',title:'年龄',width:100,align:'center'},
 					{field:'school',title:'院校',width:100,align:'center'},
 					{field:'work',title:'工作',width:100,align:'center'},
 					{field:'company',title:'公司',width:100,align:'center'},
 					{field:'city',title:'城市',width:100,align:'center'}
 			    ]],
 			    onLoadSuccess:function(data){
 			    	console.log('data===',data)
 			    }
 			});
 		});
 		
 		function add(a){
 			console.log('a===',a)
 		}
 		
 		function save(a){
 			alert(a);
 			console.log('a===',a)
 		}
 		
 	</script>
</html>