package utils;

public class UUID {

	public static void main(String[] args) {
		 System.out.println("uuid="+UUID.getuuid());
	}
	
	//生成uuid,并且取出"-",例如：（404e459ce150447eb32184a441eb5c8e）
	public static String getuuid(){
		String uuid = java.util.UUID.randomUUID().toString().replace("-", "");
		return uuid;
	}
}
