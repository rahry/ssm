package utils;

import java.util.List;

public class PageResult<T>{
	//结果集
	private List<T> rows;
	//总数
	private long total;
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
}
