package com.cn.ssm.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cn.ssm.domain.User;
import com.cn.ssm.service.IUserService;

@RestController
@RequestMapping("/userRest")
public class UserRestController {
	 @Resource
	    private IUserService userService;

	    @RequestMapping("/userShow")
	    public String toIndex(HttpServletRequest request,Model model){
	        int userId = Integer.parseInt(request.getParameter("id"));

	        User user = userService.getUserById(userId);

	        model.addAttribute("user", user);

	        return "showUser";
	    }
	    
	    /**
	     * 获取UserList
	     * @return
	     */
	    @RequestMapping(value="/topage",method = RequestMethod.GET)
	    public Map<String, Object> getUserPage(@RequestParam Map<String, String> param) {
			java.util.List<User> resultlist = new ArrayList<User>();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			int userId = Integer.parseInt(param.get("id"));
	        User user = userService.getUserById(userId);
	        resultlist.add(user);
	        resultMap.put("rows", resultlist);
	    	return resultMap;
			
		} 
	    @RequestMapping(value="/topages",method = RequestMethod.GET)
	    public Map<String, Object> getUserPages(@RequestParam Map<String, String> param) {
	    	
			return null;
			
		}
}
