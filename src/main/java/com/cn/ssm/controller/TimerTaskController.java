package com.cn.ssm.controller;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import utils.DateUtils;

@Component

public class TimerTaskController {
	@Scheduled(cron = "0 30 22 ? * *")
	 public void test1(){
	      System.out.println("job1 开始执行..."+new Date());
	  } 
	@Scheduled(cron = "0/15 * * * * ?")//每隔5秒隔行一次 
	 public void test2(){
	     System.out.println("job2 开始执行"+DateUtils.getStringDate());
	  } 
}
