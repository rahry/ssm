package com.cn.ssm.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cn.ssm.dao.UserMapper;
import com.cn.ssm.domain.User;
import com.cn.ssm.service.IUserService;

@Service
@Transactional
public class UserServiceImpl implements IUserService {
	
	@Resource
    public UserMapper userMapper;
	public User getUserById(int userId) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(userId);
	}

}
