package com.cn.ssm.service;

import com.cn.ssm.domain.User;

public interface IUserService {
	public User getUserById(int userId);

}
